#ifndef STM32L0538_DISCO_H
#define STM32L0538_DISCO_H

#include <libopencm3-plus/hw-accesories/gde021a1.h>

void gde021a1_spi_setup(Gde021a1SPI spi_conf);
void gde021a1_setup(void);
void leds_init(void);
void spi1_epaper_setup(void);

#endif // STM32L0538_DISCO_H
