/*
 * Copyright (C) 2013 ARCOS-Lab Universidad de Costa Rica
 * Author: Federico Ruiz Ugalde <memeruiz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USART_COMMON_H
#define USART_COMMON_H

#include <libopencm3-plus/newlib/syscall.h>
#include <libopencm3-plus/utils/data_structs.h>

#define IRQ_PRI_USART		(2 << 4)

int usart_open(const char *path, int flags, int mode);

int usart_close(int fd);

void usart_write_now(char *buf, int len);

long usart_write(int fd, const char *ptr, int len);

long usart_read(int fd, char *ptr, int len);

void usart_init(void);

int usart_in_poll(int fd);

extern devoptab_t dotab_usart;
extern uint32_t usart_port;
extern cbuf_t usart_cbuf_in;

#endif //USART_COMMON
