/*
 * Copyright (C) 2013 ARCOS-Lab Universidad de Costa Rica
 * Author: Federico Ruiz Ugalde <memeruiz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYSCALL_H
#define SYSCALL_H

#include <stdio.h>
//#include <libopencm3-plus/cdcacm_one_serial/cdcacm_common.h>
//#include <libopencm3-plus/cdcacm_one_serial/null_common.h>

int lo_poll(FILE *f);

typedef struct {
  const char *name;
  int  (*open)( const char *path, int flags, int mode );
  int  (*close)( int fd );
  long (*write)( int fd, const char *ptr, int len );
  long (*read)( int fd, char *ptr, int len );
  int (*poll)( int fd);
} devoptab_t;

extern devoptab_t *devoptab_list[];

#endif //SYSCALL_H
