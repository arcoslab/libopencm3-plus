/*
 * Copyright (C) 2013 ARCOS-Lab Universidad de Costa Rica
 * Written by Federico Ruiz Ugalde <memeruiz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LEDS_H
#define LEDS_H

#include <libopencm3/stm32/gpio.h>

#define LED_GREEN0_PIN GPIO13
#define LED_GREEN0_PORT GPIOB
#define LED_RED0_PIN GPIO5
#define LED_RED0_PORT GPIOC
#define LED_GREEN1_PIN GPIO13
#define LED_GREEN1_PORT GPIOG
#define LED_RED1_PIN GPIO14
#define LED_RED1_PORT GPIOG

#define LD3 LED_GREEN1_PORT, LED_GREEN1_PIN // green
#define LD4 LED_RED1_PORT, LED_RED1_PIN // red
#define LD5 LED_GREEN0_PORT, LED_GREEN0_PIN // green VBUS present
#define LD6 LED_RED0_PORT, LED_RED0_PIN // red VBUS overcurrent

#define LGREEN LD3
#define LRED LD4
#define LVBUS LD5
#define LVBUSOVER LD6

void leds_init(void);

#endif // LEDS_H
