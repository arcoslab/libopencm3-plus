#ifndef GDE021A1_H
#define GDE021A1_H

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>

/* // Formulas */

/* // Commands */

/* // Registers */

/* // Flags */


typedef struct {
  uint32_t spi;
  uint32_t spicsport;
  uint16_t spi_cs;
  uint32_t spi_port;
  uint16_t spi_clk_pin;
  uint16_t spi_mosi_pin;
  uint32_t busy_port;
  uint16_t busy_pin;
  uint32_t reset_port;
  uint16_t reset_pin;
  uint32_t dc_port;
  uint16_t dc_pin;
  uint32_t pwren_port;
  uint16_t pwren_pin;
  double fxo;
} Gde021a1SPI;

// General setting up functions
void init_gde021a1_spi(Gde021a1SPI dev);
void init_gde021a1(Gde021a1SPI dev);

// Utils functions
/* void _update_bitfield_gde021a1(Gde021a1SPI dev, uint8_t reg, uint8_t bitfield, */
/*                       uint8_t value); */

/* uint8_t _get_bitfield_gde021a1(Gde021a1SPI dev, uint8_t reg, uint8_t bitfield); */
uint8_t gde021a1_cmd_data(Gde021a1SPI spi_conf, uint8_t cmd,
                   uint8_t *wr_data, uint8_t count);
uint8_t gde021a1_data(Gde021a1SPI spi_conf,
                   uint8_t *wr_data, uint8_t count);

uint16_t gde021a1_busy(Gde021a1SPI spi_conf);

#endif // GDE021A1_H
