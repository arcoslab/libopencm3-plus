
#define LAST_REGISTER_ADDR 0x0B

#define MAX31856_WRITE (1<<7)

#define MAX31856_CR0 0x00
#define MAX31856_CR0_CMODE (1<<7)
#define MAX31856_CR1 0x01
#define MAX31856_CR1_AVGSEL_bit 4
#define MAX31856_LTCBH 0x0C
#define MAX31856_SR 0x0F

void max31856_init(uint32_t spi, uint32_t port, uint16_t pin);
void max31856_write_register(uint32_t spi_id, uint32_t cs_port,
                                    uint16_t cs_pin, uint8_t reg_addr,
                             uint8_t data);
void max31856_write_registers(uint32_t spi_id, uint32_t cs_port,
                                     uint16_t cs_pin, uint8_t reg_addr,
                              uint8_t n_write, const uint8_t *data);
uint8_t max31856_read_register(uint32_t spi_id, uint32_t cs_port,
                               uint16_t cs_pin, uint8_t reg_addr);
void max31856_read_registers(uint32_t spi_id, uint32_t cs_port,
                             uint16_t cs_pin, uint8_t reg_addr,
                             uint8_t count, uint8_t *response);
uint8_t max31856_read_temperature_and_status(uint32_t spi_id,
                                             uint32_t cs_port,
                                             uint16_t cs_pin,
                                             float *temperature);
void max31856_print_all_registers(uint32_t spi_id, uint32_t cs_port,
                                  uint16_t cs_pin);
