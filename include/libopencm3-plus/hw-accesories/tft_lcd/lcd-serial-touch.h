/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2014 Chuck McManis <cmcmanis@mcmanis.com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LCD_SERIAL_TOUCH_H
#define __LCD_SERIAL_TOUCH_H

// Reference file:
// https://datasheet.lcsc.com/szlcsc/STMicroelectronics-STMPE811QTR_C118017.pdf

#include <libopencm3-plus/hw-accesories/tft_lcd/i2c-lcd-touch.h>

typedef enum { X_COORD, Y_COORD, Z_COORD } tft_coord_t;

/////////////////////////////////////////////////////////////////////
// Touchscreen Controller Control
/////////////////////////////////////////////////////////////////////

// Operation mode, axis acquisition
typedef enum {
  TSC_XYZ_AXIS,
  TSC_XY_AXIS,
  TSC_X_AXIS,
  TSC_Y_AXIS,
  TSC_Z_AXIS
} tsc_control_opmode_t;

// Determines the distance between the current touch position and the previous
// touch position. If the distance is shorter than the tracking index, it is
// discarded.
// (Current X - Previously Reported X) + (Current Y - Previously Reported Y) >
// Tracking Index
typedef enum {
  TRACK_NONE, // No window tracking
  TRACK_4,
  TRACK_8,
  TRACK_16,
  TRACK_32,
  TRACK_64,
  TRACK_92,
  TRACK_127
} tsc_control_track_t;

/////////////////////////////////////////////////////////////////////
// Touchscreen Controller Configuration
/////////////////////////////////////////////////////////////////////

// Sampling time sequence
// Touch detect DELAY, Settling Time, Sampling X, Settling Time, Sampling Y,
// Settling Time, Sampling Z, Touch Detect Delay

typedef enum {
  MICRO_S_10_SET,
  MICRO_S_100_SET,
  MICRO_S_500_SET,
  MILLI_S_1_SET,
  MILLI_S_5_SET,
  MILLI_S_10_SET,
  MILLI_S_50_SET,
  MILLI_S_100_SET
} tsc_configuration_settling_t;

typedef enum {
  MICRO_S_10_D, // 10 micro s
  MICRO_S_50_D,
  MICRO_S_100_D,
  MICRO_S_500_D,
  MILLI_S_1_D,
  MILLI_S_5_D,
  MILLI_S_10_D,
  MILLI_S_50_D
} tsc_configuration_delay_t;

// The touchscreen controller can be configurated to
// oversampling by 2/4/8 times and provide the average
// value as final output
typedef enum {
  SAMPLE_1,
  SAMPLE_2,
  SAMPLE_4,
  SAMPLE_8
} tsc_configuration_ave_ctrl_t;

void tft_convert_touch_coord_to_lcd_coord(const int, const int, int *, int *);
uint16_t tft_identify_check_device(void);
uint8_t tft_get_id_verification(void);

/*
 * Generic touch-screen setup
 *
 *     Operation mode: X,Y,Z Axis
 *     Average sample: 2
 */
void tft_setup(void);

/*
 * Enable/Disable all clocks
 * Clocks: ADC, touchscreen controller, GPIO, temperature sensor
 *
 * NOTE: This function manage all the clocks at same time,
 * see reference file to know how to manage each clock
 *
 * IMPORTANT: Disable the clocks is the first action to
 * programming the touch screen
 *
 * Parameters:
 *     bool: true to enable, false to disable
 */
void tft_enable_clocks(const bool);
uint8_t tft_get_clock_status(void);

/*
 * Set the controller control configuration whitout apply it
 *
 * Parameters:
 *     tsc_control_opmode_t: (defined above)
 *     tsc_control_track_t: (defined above)
 */
void tft_touchscreen_configure_controller_control(const tsc_control_opmode_t,
                                                  const tsc_control_track_t);
/*
 * Apply the controller control configuration if it has been setting
 *
 * Parameters:
 *     bool: enable the controller if it's true, desable if it's false
 */
void tft_touchscreen_enable_controller_control(const bool);
uint8_t tft_get_controller_control_status(void);

/*
 * Set the controller configuration
 *
 * Parameters:
 *     tsc_configuration_settling_t: (defined above)
 *     tsc_configuration_delay_t: (defined above) time before and after
 *       sampling the axis values
 *     tsc_configuration_ave_ctrl_t: (defined above) oversampling times
 *       to get the final averaged value
 */
void tft_touchscreen_apply_controller_configuration(
    const tsc_configuration_settling_t, const tsc_configuration_delay_t,
    const tsc_configuration_ave_ctrl_t);
uint8_t tft_get_touchscreen_controller_configuration(void);

void tft_set_fifo_level_interrupt(const uint8_t);
uint8_t tft_get_fifo_interrupt_level(void);
uint8_t tft_get_fifo_size(void);

void tft_get_coord_data_access(const tft_coord_t, uint16_t *);

bool tft_is_touch_detected(void);

// TODO: [brjmm] function implement the following functions
// void tft_reset_touchscreen_controller(void);
// void tft_hibernate_touchscreen_controller(void);

#endif /* __LCD_SERIAL_TOUCH_H */
