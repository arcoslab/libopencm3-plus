/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2010 Thomas Otto <tommi@viadmin.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __I2C_LCD_TOUCH_H
#define __I2C_LCD_TOUCH_H

#include <libopencm3/stm32/i2c.h>

// The controller for the TFT LCD (Thin-film-transistor liquid-crystal display)
// is the STMPE811QTR
// https://datasheet.lcsc.com/szlcsc/STMicroelectronics-STMPE811QTR_C118017.pdf
#define STMPE811_I2C I2C3
#define STMPE811_ADDR 0x41

void i2c_init(void);

#endif /* __I2C_LCD_TOUCH_H */
