/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2014 Chuck McManis <cmcmanis@mcmanis.com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LCD_SPI_H
#define __LCD_SPI_H

#include <stdint.h>

#include <libopencm3-plus/hw-accesories/lcd/gfx.h>

/* Color definitions */
#define LCD_BLACK GFX_COLOR_BLACK
#define LCD_WHITE GFX_COLOR_WHITE
#define LCD_BLUE GFX_COLOR_BLUE
#define LCD_BLUE2 GFX_COLOR_BLUE2
#define LCD_RED GFX_COLOR_RED
#define LCD_GREEN GFX_COLOR_GREEN
#define LCD_CYAN GFX_COLOR_CYAN
#define LCD_MAGENTA GFX_COLOR_MAGENTA
#define LCD_YELLOW GFX_COLOR_YELLOW
#define LCD_GREY GFX_COLOR_GREY

#define LCD_CS PC2   /* CH 1 */
#define LCD_SCK PF7  /* CH 2 */
#define LCD_DC PD13  /* CH 4 */
#define LCD_MOSI PF9 /* CH 3 */

#define LCD_SPI SPI5

#define LCD_WIDTH GFX_WIDTH
#define LCD_HEIGHT GFX_HEIGHT

#define FRAME_SIZE (LCD_WIDTH * LCD_HEIGHT)
#define FRAME_SIZE_BYTES (FRAME_SIZE * 2)

#define LCD_TEXT_MIN_SIZE 1
#define LCD_TEXT_MID_SIZE 2
#define LCD_TEXT_MAX_SIZE 3

/*
 * prototypes for the LCD example
 *
 * This is a very basic API, initialize, a function which will show the
 * frame, and a function which will draw a pixel in the framebuffer.
 */

void lcd_spi_init(void);
void lcd_show_frame(void);
void lcd_draw_pixel(int x, int y, uint16_t color);

#endif /* __LCD_SPI_H */
