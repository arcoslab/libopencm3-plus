/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2014 Chuck McManis <cmcmanis@mcmanis.com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Now this is just the clock setup code from systick-blink as it is the
 * transferrable part.
 */

/*
 * This include file describes the functions exported by clock.c
 */
#ifndef __CLOCK_H
#define __CLOCK_H

#include <stdint.h>

/*
 * Definitions for functions being abstracted out
 */
void msleep(uint32_t);
uint32_t mtime(void);
void clock_setup(void);
void mtime_reset(void);

// Recomended clock setup
/* const uint32_t one_milisecond_rate = 168000; */
/* rcc_clock_setup_hse_3v3(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]); */

/* /\* clock rate / 168000 to get 1mS interrupt rate *\/ */
/* systick_set_reload(one_milisecond_rate); */
/* systick_set_clocksource(STK_CSR_CLKSOURCE_AHB); */
/* systick_counter_enable(); */

/* /\* this done last *\/ */
/* systick_interrupt_enable(); */

#endif /* __CLOCK_H */
