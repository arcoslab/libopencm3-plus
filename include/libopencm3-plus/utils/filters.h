typedef struct Avg_filter {
  float *data;
  int size;
} avg_filter_t;

avg_filter_t avg_filter_create_f(int size);
float avg_filter_f(avg_filter_t *avg_filter_data, float data);
