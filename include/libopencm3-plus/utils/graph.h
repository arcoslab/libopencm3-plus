#include <stdint.h>
#include <stdbool.h>

#ifndef GRAPH_H
#define GRAPH_H

typedef struct Point {
  float y;
  float x;
} point_t;

typedef struct Graph {
  uint16_t pixel_width;
  uint16_t pixel_height;
  point_t *points;
  uint16_t n_points;
  uint16_t max_points;
  float x_max;
  float x_min;
  float y_max;
  float y_min;
  float x_notch;
  float y_notch;
  uint16_t color;
} graph_t;

void graph_create(uint16_t pixel_width,
                  uint16_t pixel_height,
                  graph_t *graph, uint16_t color,
                  uint16_t max_points);
bool graph_add_point(float x, float y, graph_t *graph);
uint16_t p2pix(float data, float data_per_pixel);
void graph_plot(graph_t *graph, uint16_t x_pixel_pos,
                uint16_t y_pixel_pos, float x_div,
                float y_div);
void graph_draw_borders(graph_t *graph, uint16_t x_pixel_pos,
                        uint16_t y_pixel_pos, uint16_t
                        x_pixel_div, uint16_t y_pixel_div,
                        float x_per_pixel, float y_per_pixel);
void graph_prune_half(graph_t *graph);
void graph_prune_half2(graph_t *graph);
void graph_destroy(graph_t *graph);

#endif // GRAPH_H
