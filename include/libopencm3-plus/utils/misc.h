/*
 * Copyright (C) 2013 ARCOS-Lab Universidad de Costa Rica
 * Written by Federico Ruiz Ugalde <memeruiz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MISC_H
#define MISC_H
#include <stdbool.h>
#include <stdint.h>

#define degrees_to_radians(d) ((d)*6.2831853 / 360.0)

void wait(int a);

void printled(int a, int gpiop, int pin);

void printled2(int rep, int time, int gpiop, int pin);

int strlen2(char s[]);

/* reverse:  reverse string s in place */
void reverse(char s[]);

void my_itoa(int n, char s[]);

bool get_flag(uint32_t data, uint8_t position);

void set_flag_8bit(uint8_t *data, uint8_t position, bool value);

void set_flag_16bit(uint16_t *data, uint8_t position, bool value);

void set_flag_32bit(uint32_t *data, uint8_t position, bool value);


#endif // MISC_H
