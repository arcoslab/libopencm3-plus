/*
 * Copyright (C) 2011  Black Sphere Technologies Ltd.
 * Written by Gareth McMullin <gareth@blacksphere.co.nz>
 * Modified by Federico Ruiz Ugalde <memeruiz@gmail.com> ARCOS-Lab
 * Universidad de Costa Rica.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file implements a the USB Communications Device Class - Abstract
 * Control Model (CDC-ACM) as defined in CDC PSTN subclass 1.2.
 *
 * The device's unique id is used as the USB serial number string.
 */

#include <stdlib.h>

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/usart.h>

#include <libopencm3-plus/utils/misc.h>
#include <libopencm3-plus/utils/common.h>
#include <libopencm3-plus/utils/data_structs.h>
#include <libopencm3-plus/newlib/devices/usart.h>
//#include <libopencm3-plus/steval-ids001v4m/leds.h>

#define USART_BAUDRATE 115200
#define USART_READ_BUF_SIZE 16

cbuf_t usart_cbuf_in;

void usart_init(void) {
  usart_port=USART1;
  /* Pin port A and USART2 clock */
	rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_USART1);

    /* Alternate function 7 for pins 2, 3 port A for USART1 */
    gpio_set_af(GPIOC, GPIO_AF7, GPIO4 | GPIO5);
	gpio_mode_setup(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO4 | GPIO5);

	/* Setup UART parameters. */
	usart_set_baudrate(usart_port, USART_BAUDRATE);
	usart_set_databits(usart_port, 8);
	usart_set_stopbits(usart_port, USART_STOPBITS_1);
	usart_set_mode(usart_port, USART_MODE_TX_RX);
	usart_set_parity(usart_port, USART_PARITY_NONE);
	usart_set_flow_control(usart_port, USART_FLOWCONTROL_NONE);


    // receive buffer setup
    if (cbuf_init(&usart_cbuf_in, USART_READ_BUF_SIZE) !=
        0) { // couldn't initialize buffer for usb
      while (1) {
        //printled(5, LRED);
      }
    }

    usart_enable_rx_interrupt(usart_port);
    nvic_set_priority(NVIC_USART1_EXTI25_IRQ, IRQ_PRI_USART);
    nvic_enable_irq(NVIC_USART1_EXTI25_IRQ);
	/* Finally enable the USART. */
	usart_enable(usart_port);

    //TODO: set interrupt on for receiving
}

//TODO: usart interrupt
void usart1_exti25_isr(void) {
  char data;
  if (usart_get_flag(usart_port, USART_FLAG_RXNE) != 0) {
    //There is data!
    data=usart_recv(usart_port);
    cbuf_append(&usart_cbuf_in, &data, 1);
  }
}
