/*
 * Copyright (C) 2013 ARCOS-Lab Universidad de Costa Rica
 * Written by Federico Ruiz Ugalde <memeruiz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <libopencm3/cm3/cortex.h>
#include <libopencm3-plus/utils/misc.h>
//#include <libopencm3-plus/steval-ids001v4m/leds.h>
#include <libopencm3-plus/stm32f429idiscovery/leds.h>
#include <libopencm3-plus/newlib/devices/cdcacm.h>
#include <libopencm3-plus/newlib/devices/usart.h>
#include <libopencm3-plus/newlib/devices/null.h>
#include <libopencm3-plus/utils/common.h>
#include <libopencm3-plus/utils/mutex.h>
#include <libopencm3-plus/newlib/syscall.h>

#undef errno
extern int errno;
#define HEAPSIZE 6*2048
//#define HEAPSIZE 1024

devoptab_t *devoptab_list[] = {
   &dotab_null,  /* standard input */
   &dotab_null,  /* standard output */
   &dotab_null,  /* standard error */
   0             /* terminates the list */
};

unsigned char _heap[HEAPSIZE];


long _write_r(struct _reent *r, int fd, const void *buf, size_t cnt);
long _read_r(struct _reent *r, int fd, char *buf, size_t cnt);
int _open_r(struct _reent *r, const char *file, int flags, int mode);
long _close_r(struct _reent *r, int fd);
caddr_t _sbrk_r(struct _reent *r, int incr);
int _stat_r(struct _reent *r, const char *file, struct stat *pstat);
int _fstat_r(struct _reent *r, int fd, struct stat *pstat);
off_t _lseek_r(struct _reent *r, int fd, off_t pos, int whence);

long _write_r(NOT_USED struct _reent *r, int fd, const void *buf, size_t cnt) {
  return (*devoptab_list[fd]).write(fd, buf, cnt);
}

long _read_r(NOT_USED struct _reent *r, int fd, char *buf, size_t cnt) {
  return (*devoptab_list[fd]).read(fd, buf, cnt);
}

int _open_r(NOT_USED struct _reent *r, const char *file, int flags, int mode) {
  int which_devoptab = 0;
  int fd = -1;
  /* search for "file" in dotab_list[].name */
  do {
    if( strcmp( (*devoptab_list[which_devoptab]).name, file ) == 0 ) {
      fd = which_devoptab;
      break;
    }
  } while( devoptab_list[which_devoptab++] );
  /* if we found the requested file/device,
     then invoke the device's open_r() method */
  if( fd != -1 ) (*devoptab_list[fd]).open(file, flags, mode );
  /* it doesn't exist! */
  else errno = ENODEV;
  return fd;
}

long _close_r(NOT_USED struct _reent *r, int fd) {
  return (*devoptab_list[fd]).close(fd);
}

caddr_t _sbrk_r(NOT_USED struct _reent *r, int incr) {
  static unsigned char *heap_end;
  unsigned char *prev_heap_end;
  /* initialize */
  if( heap_end == 0 ) heap_end = _heap;
  prev_heap_end = heap_end;
  if( heap_end + incr -_heap > HEAPSIZE ) {
    /* heap overflow—announce on stderr */
    //    printled(10, LRED);
    //    printled(10, LORANGE);
    //write( 2, "Heap overflow!\n", 15 );
    abort();
  }
  heap_end += incr;
  return (caddr_t) prev_heap_end;
}


int _stat_r(NOT_USED struct _reent *r, NOT_USED const char *file, struct stat *pstat) {
  pstat->st_mode = S_IFCHR;
  return 0;
}


int _fstat_r(NOT_USED struct _reent *r, NOT_USED int fd, struct stat *pstat) {
  pstat->st_mode = S_IFCHR;
  return 0;
}


off_t _lseek_r(NOT_USED struct _reent *r, NOT_USED int fd, NOT_USED off_t pos, NOT_USED int whence) {
   return 0;
}

int lo_poll(FILE *f) {
  return (*devoptab_list[fileno(f)]).poll(fileno(f));
}
