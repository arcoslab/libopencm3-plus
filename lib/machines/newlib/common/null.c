/*
 * Copyright (C) 2011  Black Sphere Technologies Ltd.
 * Written by Gareth McMullin <gareth@blacksphere.co.nz>
 * Modified by Federico Ruiz Ugalde <memeruiz@gmail.com> ARCOS-Lab
 * Universidad de Costa Rica.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Null dummy serial interface
 */

//#include <libopencm3-plus/stm32f4discovery/leds.h>
//#include <libopencm3-plus/steval-ids001v4m/leds.h>
//#include <libopencm3-plus/utils/common.h>
//#include <libopencm3-plus/utils/data_structs.h>
//#include <libopencm3-plus/utils/misc.h>
#include <stdlib.h>
#include <libopencm3-plus/utils/common.h>
#include <libopencm3-plus/newlib/devices/null.h>


devoptab_t dotab_null = { "null",
  null_open,
  null_close,
  null_write,
  null_read,
  null_poll};

int null_open(NOT_USED const char *path, NOT_USED int flags,
                NOT_USED int mode) {
  return (0);
}

int null_close(NOT_USED int fd) { return (0); }

long null_write(NOT_USED int fd, NOT_USED const char *ptr, NOT_USED int len) {
  return (0);
}

long null_read(NOT_USED int fd, NOT_USED char *ptr, NOT_USED int len) {
  return (0);
}

int null_poll(NOT_USED int fd) { return (0); }
