/*
 * Copyright (C) 2013 ARCOS-Lab Universidad de Costa Rica
 * Author: Federico Ruiz Ugalde <memeruiz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * USART device for newlib access (printf and family)
 */

#include <libopencm3/stm32/usart.h>
#include <libopencm3-plus/newlib/devices/usart.h>
//#include <libopencm3-plus/stm32f4discovery/leds.h>
//#include <libopencm3-plus/steval-ids001v4m/leds.h>
#include <libopencm3-plus/utils/common.h>
#include <libopencm3-plus/utils/data_structs.h>
#include <libopencm3-plus/utils/misc.h>
#include <libopencm3/cm3/nvic.h>
#include <stdlib.h>

devoptab_t dotab_usart = { "usart",
  usart_open,
  usart_close,
  usart_write,
  usart_read,
  usart_in_poll};

uint32_t usart_port;

int usart_open(NOT_USED const char *path, NOT_USED int flags,
                NOT_USED int mode) {
  return (0);
}

int usart_close(NOT_USED int fd) { return (0); }

void usart_write_now(char *buf, int len) {
	int i;
    for (i = 0; i < len; i++)
      usart_send_blocking(usart_port, buf[i]);
}

#define USART_PACKET_SIZE 64

long usart_write(NOT_USED int fd, const char *ptr, int len) {
  // printled2(1, 10, LGREEN);
  int index;
  static char buf[USART_PACKET_SIZE];
  static int buf_pos = 0;
  /* For example, output string by UART */
  for (index = 0; index < len; index++) {
    buf[buf_pos] = ptr[index];
    buf_pos += 1;
    if (buf_pos == 1) {
      // if (buf_pos == CDCACM_PACKET_SIZE/2) {
      usart_write_now(buf, buf_pos);
      buf_pos = 0;
    }
  }

  return len;
}

long usart_read(NOT_USED int fd, char *ptr, int len) {
  while (cbuf_used(&usart_cbuf_in) < len) {
  };
  return (cbuf_pop(&usart_cbuf_in, ptr, len));
}

int usart_in_poll(int fd) {
  if (fd == 0) { // std
    return (cbuf_used(&usart_cbuf_in));
  } else {
    return (-1);
  }
}

