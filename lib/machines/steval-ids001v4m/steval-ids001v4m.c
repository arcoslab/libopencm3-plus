/*
 * Copyright (C) 2013 ARCOS-Lab Universidad de Costa Rica
 * Author: Federico Ruiz Ugalde <memeruiz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <libopencm3-plus/hw-accesories/spirit1.h>

#include <libopencm3-plus/utils/misc.h>
#include <libopencm3-plus/steval-ids001v4m/steval-ids001v4m.h>
#include <libopencm3-plus/steval-ids001v4m/leds.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/spi.h>


#define SPSGRF_SDN_PORT GPIOC
#define SPSGRF_SDN GPIO13
#define SPSGRF_SPI_PORT GPIOB
#define SPSGRF_SPI SPI2
#define SPSGRF_CS_PORT GPIOB
#define SPSGRF_CS GPIO12

SpiritSPI spsgrf_spi = {
  .spiport = SPI2,
  .spicsport = GPIOB,
  .spi_cs = GPIO12,
  .sdnport = GPIOC,
  .sdnpin = GPIO13,
  .gpio0port = GPIOB,
  .gpio1port = GPIOB,
  .gpio2port = GPIOB,
  .gpio3port = GPIOB,
  .gpio0pin = GPIO7,
  .gpio1pin = GPIO8,
  .gpio2pin = GPIO9,
  .gpio3pin = GPIO10,
  .fxo = 50000000,
};

/* uint16_t spsgrf_write(uint8_t reg_addr, uint8_t *wr_data, uint8_t
 * count) { */
/*   //Write to spirit1 register(s). Multiple write if count>1 */
/*   //Returns Spirit1 status word */

/*   return(sp1_write(reg_addr, wr_data, count, spsgrf_spi)); */
/* } */

/* uint16_t spsgrf_cmd(uint8_t cmd) { */
/*   //Command spirit1 */
/*   //Returns Spirit1 status word */

/*   return(sp1_cmd(cmd, spsgrf_spi)); */
/* } */

/* uint16_t spsgrf_read(uint8_t reg_addr, uint8_t *rd_data, uint8_t
 * count, bool inv_dir) { */
/*   //Read from spirit1 register(s). Multiple read if count>1 */
/*   //Returns Spirit1 status word */

/*   return(sp1_read(reg_addr, rd_data, count, spsgrf_spi, inv_dir));
 */
/* } */

void sp1_spi_setup(SpiritSPI spi_conf) {
  // Setups spi for spirit1
  // gpios are MISO, MOSI and SCLK (or-d)
  // Before calling this function you have to:
  //- Turn the clocks on for the GPIO port and SPI unit
  //- Setup CS as output (gpio_mode_setup)
  //- Configure Alternative function number for SPI (MISO, MOSI,
  // SCLK)
  // pins (gpio_set_af)
  //- Configure MISO, MOSI, SCLK pins as Alternate Function mode
  //(gpio_mode_setup)

  /* Reset SPI, SPI_CR1 register cleared, SPI is disabled */
  spi_reset(spi_conf.spiport);

  /* Set up SPI in Master mode with:
   * Clock baud rate: 1/256 of peripheral clock frequency
   (125kHz)
   * Clock polarity: Idle LOW
   * Clock phase: Data valid on 1nd clock pulse
   * Data frame format: 8-bit
   * Frame format: MSB First
   */
  spi_init_master(spi_conf.spiport, SPI_CR1_BAUDRATE_FPCLK_DIV_64,
                  SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE,
                  SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_8BIT,
                  SPI_CR1_MSBFIRST);

  /*
   * Set NSS management to software.
   *
   * Note:
   * Setting nss high is very important, even if we are controlling
   * the GPIO ourselves this bit needs to be at least set to 1,
   * otherwise the spi peripheral will not send any data out.
   */
  spi_enable_software_slave_management(spi_conf.spiport);
  spi_set_nss_high(spi_conf.spiport);

  /* Enable SPI1 periph. */
  spi_enable(spi_conf.spiport);
}


void spi_setup(void) {
  /* Pin port B and SPI2 clock */
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_SPI2);

  /* Alternate Function 5 for SPI on port B */
  gpio_set_af(SPSGRF_SPI_PORT, GPIO_AF5, GPIO13 | GPIO14 | GPIO15);
  /* AF for pins 13(SCK), 14(MISO), 15(MOSI) on port B */
  gpio_mode_setup(SPSGRF_SPI_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE,
                  GPIO13 | GPIO14 | GPIO15);

  /* Errata: section 2.4.6, increase pin speed or decrease APB clock
   * speed */
  /* This is necessary for SPI and I2S */
  gpio_set_output_options(SPSGRF_SPI_PORT, GPIO_OTYPE_PP,
                          GPIO_OSPEED_40MHZ,
                          GPIO13 | GPIO14 | GPIO15);

  /* Output mode for pin 12 (spsgrf-868 SPI_CS connection) */
  gpio_mode_setup(SPSGRF_CS_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE,
                  spsgrf_spi.spi_cs);

  gpio_set(spsgrf_spi.spicsport, spsgrf_spi.spi_cs);

  sp1_spi_setup(spsgrf_spi);
}

void spsgrf868_setup(void) {
  // Configuring SDN pin
  rcc_periph_clock_enable(RCC_GPIOC);
  gpio_mode_setup(spsgrf_spi.sdnport, GPIO_MODE_OUTPUT,
                  GPIO_PUPD_NONE, spsgrf_spi.sdnpin);
  gpio_set(spsgrf_spi.sdnport, spsgrf_spi.sdnpin);
  wait(100);
  gpio_clear(spsgrf_spi.sdnport, spsgrf_spi.sdnpin);
  wait(100);

  // Configuring spsgrf GPIO pins
  rcc_periph_clock_enable(RCC_GPIOB);
  gpio_mode_setup(spsgrf_spi.gpio0port, GPIO_MODE_INPUT,
                  GPIO_PUPD_NONE, spsgrf_spi.gpio0pin);
  gpio_mode_setup(spsgrf_spi.gpio1port, GPIO_MODE_INPUT,
                  GPIO_PUPD_NONE, spsgrf_spi.gpio1pin);
  gpio_mode_setup(spsgrf_spi.gpio2port, GPIO_MODE_INPUT,
                  GPIO_PUPD_NONE, spsgrf_spi.gpio2pin);
  gpio_mode_setup(spsgrf_spi.gpio3port, GPIO_MODE_INPUT,
                  GPIO_PUPD_NONE, spsgrf_spi.gpio3pin);
}

void leds_init(void) {
  /* GPIOB for LEDs */
  rcc_periph_clock_enable(RCC_GPIOB);

  /* GPIO0 y 1 for LEDs (RED and ORANGE respectively) */
  gpio_mode_setup(LEDS_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE,
                  GPIO0 | GPIO1);
}


void eeprom_init(void) {
  rcc_periph_clock_enable(RCC_GPIOA);

  /* GPIO0 y 1 for LEDs (RED and ORANGE respectively) */
  gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO9);
  gpio_set(GPIOA, GPIO9);
}
