/*
 * Copyright (C) 2013 ARCOS-Lab Universidad de Costa Rica
 * Author: Federico Ruiz Ugalde <memeruiz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <libopencm3-plus/hw-accesories/gde021a1.h>
#include <libopencm3-plus/utils/misc.h>
#include <libopencm3-plus/stm32l0538-disco/stm32l0538-disco.h>
#include <libopencm3-plus/stm32l0538-disco/leds.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/spi.h>


#define SPSGRF_SDN_PORT GPIOC
#define SPSGRF_SDN GPIO13
#define GDE021A1_SPI_PORT GPIOB
#define GDE021A1_SPI SPI1
#define GDE021A1_CS_PORT GPIOA
#define GDE021A1_CS GPIO15

Gde021a1SPI gde021a1_spi = {
  .spi = SPI1,
  .spicsport = GPIOA,
  .spi_cs = GPIO15,
  .spi_port = GPIOB,
  .spi_clk_pin= GPIO3,
  .spi_mosi_pin= GPIO5,
  .busy_port = GPIOA,
  .busy_pin = GPIO8,
  .reset_port = GPIOB,
  .reset_pin = GPIO2,
  .dc_port = GPIOB,
  .dc_pin = GPIO11,
  .pwren_port = GPIOB,
  .pwren_pin = GPIO10,
  .fxo = 32000000,
};

void gde021a1_spi_setup(Gde021a1SPI spi_conf) {
  // Setups spi for gde021a1
  // gpios are MOSI and SCLK (or-d)
  // Before calling this function you have to:
  //- Turn the clocks on for the GPIO port and SPI unit
  //- Setup CS as output (gpio_mode_setup)
  //- Configure Alternative function number for SPI (MOSI,
  // SCLK)
  // pins (gpio_set_af)
  //- Configure MOSI, SCLK pins as Alternate Function mode
  //(gpio_mode_setup)

  /* Reset SPI, SPI_CR1 register cleared, SPI is disabled */
  spi_reset(spi_conf.spi);

  /* Set up SPI in Master mode with:
   * Clock baud rate: 1/256 of peripheral clock frequency
   (125kHz)
   * Clock polarity: Idle LOW
   * Clock phase: Data valid on 1nd clock pulse
   * Data frame format: 8-bit
   * Frame format: MSB First
   */
  spi_init_master(spi_conf.spi, SPI_CR1_BAUDRATE_FPCLK_DIV_64,
                  SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE,
                  SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_8BIT,
                  SPI_CR1_MSBFIRST);

  /*
   * Set NSS management to software.
   *
   * Note:
   * Setting nss high is very important, even if we are controlling
   * the GPIO ourselves this bit needs to be at least set to 1,
   * otherwise the spi peripheral will not send any data out.
   */
  spi_enable_software_slave_management(spi_conf.spi);
  spi_set_nss_high(spi_conf.spi);

  /* Enable SPI1 periph. */
  spi_enable(spi_conf.spi);
}


void spi1_epaper_setup(void) {
  /* Pin port B and SPI1 clock */
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_SPI1);

  /* Alternate Function 0 for SPI on port B */
  gpio_set_af(gde021a1_spi.spi_port, GPIO_AF0, gde021a1_spi.spi_clk_pin | gde021a1_spi.spi_mosi_pin);
  /* AF for pins 3(SCK), 5(MOSI) on port B */
  gpio_mode_setup(gde021a1_spi.spi_port, GPIO_MODE_AF, GPIO_PUPD_NONE,
                  gde021a1_spi.spi_clk_pin | gde021a1_spi.spi_mosi_pin);

  /* Output mode for pin 15 (SPI_CS connection) */
  gpio_mode_setup(gde021a1_spi.spicsport, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE,
                  gde021a1_spi.spi_cs);

  gpio_set(gde021a1_spi.spicsport, gde021a1_spi.spi_cs);

  gde021a1_spi_setup(gde021a1_spi);
}

void gde021a1_setup(void) {
  //busy
  rcc_periph_clock_enable(RCC_GPIOA);
  gpio_mode_setup(gde021a1_spi.busy_port, GPIO_MODE_INPUT,
                  GPIO_PUPD_NONE, gde021a1_spi.busy_pin);

  // Configuring reset pin
  rcc_periph_clock_enable(RCC_GPIOB);
  gpio_mode_setup(gde021a1_spi.reset_port, GPIO_MODE_OUTPUT,
                  GPIO_PUPD_NONE, gde021a1_spi.reset_pin);
  // Configuring d/c pin
  rcc_periph_clock_enable(RCC_GPIOB);
  gpio_mode_setup(gde021a1_spi.dc_port, GPIO_MODE_OUTPUT,
                  GPIO_PUPD_NONE, gde021a1_spi.dc_pin);
  // Configuring power en pin
  rcc_periph_clock_enable(RCC_GPIOB);
  gpio_mode_setup(gde021a1_spi.pwren_port, GPIO_MODE_OUTPUT,
                  GPIO_PUPD_NONE, gde021a1_spi.pwren_pin);

  gpio_set(gde021a1_spi.pwren_port, gde021a1_spi.pwren_pin);
  wait(1);
  gpio_clear(gde021a1_spi.pwren_port, gde021a1_spi.pwren_pin);
  wait(1);
  gpio_clear(gde021a1_spi.reset_port, gde021a1_spi.reset_pin);
  wait(1);
  gpio_set(gde021a1_spi.reset_port, gde021a1_spi.reset_pin);
  wait(1);
  gpio_set(gde021a1_spi.dc_port, gde021a1_spi.dc_pin);
  wait(1);
}

void leds_init(void) {
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);

  rcc_peripheral_enable_clock(&RCC_IOPENR, RCC_IOPENR_IOPAEN);
  gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO5);
  rcc_peripheral_enable_clock(&RCC_IOPENR, RCC_IOPENR_IOPBEN);
  gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO4);
}
