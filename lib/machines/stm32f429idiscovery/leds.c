#include <libopencm3-plus/stm32f429idiscovery/leds.h>
#include <libopencm3-plus/utils/misc.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

void leds_init(void) {
  rcc_periph_clock_enable(RCC_GPIOG);
  gpio_mode_setup(LED_GREEN1_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, LED_GREEN1_PIN);
  rcc_periph_clock_enable(RCC_GPIOG);
  gpio_mode_setup(LED_RED1_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, LED_RED1_PIN);
}
