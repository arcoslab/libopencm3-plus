# machines

Specific code for development boards

## notes:
- ICs that are part of these devboard are not to be here
because then can appear later on other boards again
and can also be connected outside of any other devboard. But
you could include configuration specific code here (PORTS
PINS connections) for such ICs if possible. Specific code for
the IC youd still go to the hw-accesories directory
- LED configuration and initialization attached to these
boards _are_ to be here
- Clock, RCC and stm32 devboard specific configuration can be here:
  (crystals and clocks configurations)

# hw-accesories

Code for optional devices/ICs should go here

## Notes:
- Try to program flexible pin configuration on the
initialization/configuration functions

## hw-accesories/cm3

Code for stm32 core-M3 specific code. In particular,
systimer code is here for now. Other things can go here later

## hw-accesories/lcd and tft_lcd

Code for display screen management is here. Generic and
hardware specific

# utils

Software utils:
- datastructures
- software filters
- mutexes utils
- plot functions
