#include <stdio.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3-plus/hw-accesories/hw-misc.h>



uint16_t my_spi_xfer(uint32_t spi, uint16_t data) {
  spi_write(spi, data);
  /* Wait for read data ready. */
  while (!(SPI_SR(spi) & SPI_SR_RXNE))
    ;
  /* Wait for transmit finished. */
  while (!(SPI_SR(spi) & SPI_SR_TXE))
    ;
  return (SPI_DR(spi));
}


