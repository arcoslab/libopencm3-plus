/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2014 Chuck McManis <cmcmanis@mcmanis.com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Now this is just the clock setup code from systick-blink as it is the
 * transferrable part.
 */

#include <stdint.h>

#include <libopencm3/cm3/systick.h>
#include <libopencm3-plus/hw-accesories/cm3/clock.h>


/* milliseconds since boot */
static volatile uint32_t system_millis;

void sys_tick_handler(void);

/* Called when systick fires */
void sys_tick_handler(void) { system_millis++; }

/* simple sleep for delay milliseconds */
void msleep(uint32_t delay) {
  uint32_t wake = system_millis + delay;
  while (wake > system_millis)
    ;
}

/* Getter function for the current time */
uint32_t mtime(void) { return system_millis; }

void mtime_reset(void) {
  systick_interrupt_disable();
  system_millis=0;
  systick_interrupt_enable();
}
