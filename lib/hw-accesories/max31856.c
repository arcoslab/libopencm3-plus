#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>
#include <libopencm3-plus/hw-accesories/max31856.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

uint8_t backup[]={
  0x00, 0x03, 0xFF, 0x7F,
  0xC0, 0x7F, 0xFF, 0x80,
  0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00};

void max31856_init(uint32_t spi, uint32_t port, uint16_t pin) {

  printf("Restoring configuration registers\n");
  for (uint8_t i=0; i<0x0c; i++){
    max31856_write_register(spi, port,
                            pin, i, backup[i]);
    //max31856_write_register(spi, port,
    //                        pin, i, backup[i]);
    //max31856_write_register(spi, port,
    //                        pin, i, backup[i]);
  }
  printf("Setting continuos mode with 16 samples average\n");
  max31856_write_register(spi, port, pin, MAX31856_CR0,
                          MAX31856_CR0_CMODE);
  max31856_write_register(spi, port, pin, MAX31856_CR1,
                          0x3|(0x4 << MAX31856_CR1_AVGSEL_bit));
}

void max31856_print_all_registers(uint32_t spi_id, uint32_t cs_port,
                                  uint16_t cs_pin){
  uint8_t buffer[16];
  for (uint8_t i=0; i<16; i++) {
    buffer[i]=0;
  }
  max31856_read_registers(spi_id, cs_port, cs_pin, 0x00, 16, buffer);
  for (uint8_t i=0; i<=0x0F; i++){
    printf("Register 0x0%X, data 0x%02X\n", i, buffer[i]);
  }
}

void max31856_write_register(uint32_t spi_id, uint32_t cs_port,
                                    uint16_t cs_pin, uint8_t reg_addr,
                                    uint8_t data) {
  reg_addr = (reg_addr | MAX31856_WRITE);
  gpio_clear(cs_port, cs_pin);
  (void)spi_xfer(spi_id, reg_addr);
  while ((SPI_SR(spi_id) & SPI_SR_BSY)) {
    ;
  }
  (void)spi_xfer(spi_id, data);
  while ((SPI_SR(spi_id) & SPI_SR_BSY)) {
    ;
  }
  gpio_set(cs_port, cs_pin);
  return;
}

void max31856_write_registers(uint32_t spi_id, uint32_t cs_port,
                                     uint16_t cs_pin, uint8_t reg_addr,
                                     uint8_t n_write, const uint8_t *data) {
  // check valid write
  if ((reg_addr <= LAST_REGISTER_ADDR) &&
      ((reg_addr + n_write) <= (LAST_REGISTER_ADDR+1))) {
  } else {
    printf("Error en seleccion de registros  \n");
    return;
  }

  // set bit 7
  uint8_t reg_addr1 = (reg_addr | 0x80);
  gpio_clear(cs_port, cs_pin); // BREAKOUT_MAX31856_CS

  (void)spi_xfer(spi_id, reg_addr1);
  printf("Se envia: %X\n", reg_addr1);
  //print_hex(reg_addr1);
  while ((SPI_SR(spi_id) & SPI_SR_BSY)) {
    ;
  }
  for (uint8_t i = 0x00; i < n_write; i++) {
    (void)spi_xfer(spi_id, *(data + i));
    printf("Se envia: %X\n", *(data + i));
    //print_hex(*(data + i));
    while ((SPI_SR(spi_id) & SPI_SR_BSY)) {
      ;
    }
  }
  gpio_set(cs_port, cs_pin); // BREAKOUT_MAX31856_CS
  return;
}

uint8_t max31856_read_register(uint32_t spi_id, uint32_t cs_port,
                                      uint16_t cs_pin, uint8_t reg_addr) {
  // uint16_t buffer;
  uint8_t buffer;

  gpio_clear(cs_port, cs_pin);
  //First transfer is to send register (cmd)
  buffer = spi_xfer(spi_id, reg_addr);
  while ((SPI_SR(spi_id) & SPI_SR_BSY)) {
    ;
  }
  //Second transfer is to receive response
  buffer = spi_xfer(spi_id, 0x00); // funciona
  while ((SPI_SR(spi_id) & SPI_SR_BSY)) {
    ;
  }
  gpio_set(cs_port, cs_pin);
  return buffer;
}

void max31856_read_registers(uint32_t spi_id, uint32_t cs_port,
                             uint16_t cs_pin, uint8_t reg_addr,
                             uint8_t count, uint8_t *response) {

  gpio_clear(cs_port, cs_pin);
  //First transfer is to send starting register (cmd)
  spi_xfer(spi_id, reg_addr);
  while ((SPI_SR(spi_id) & SPI_SR_BSY)) {
    ;
  }

  //Next transfers is to receive response
  for (int i=0; i<count; i++) {
    *(response+i) = spi_xfer(spi_id, 0x00); // funciona
    while ((SPI_SR(spi_id) & SPI_SR_BSY)) {
      ;
    }
  }
  gpio_set(cs_port, cs_pin);
}


uint8_t max31856_read_temperature_and_status(uint32_t spi_id,
                                             uint32_t cs_port,
                                             uint16_t cs_pin,
                                             float *temperature) {
  uint8_t buffer[]={0,0,0,0};
  *temperature=0;
  int32_t temp_tmp=0;
  uint8_t fault_status;

  max31856_read_registers(spi_id, cs_port, cs_pin, MAX31856_LTCBH, 4, buffer);
  for (int i=0; i<3; i++) {
    temp_tmp|=(((uint32_t)buffer[i]) << 8*(2-i));
  }
  *temperature=(float) temp_tmp/pow(2,12);
  fault_status=buffer[3];
  return fault_status;
}

