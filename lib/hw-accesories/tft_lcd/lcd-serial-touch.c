/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2014 Chuck McManis <cmcmanis@mcmanis.com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>

#include <libopencm3/stm32/i2c.h>

#include <libopencm3-plus/hw-accesories/tft_lcd/i2c-lcd-touch.h> // STMPE811_I2C STMPE811_ADDR
#include <libopencm3-plus/hw-accesories/tft_lcd/lcd-serial-touch.h>
#include <libopencm3-plus/hw-accesories/lcd/lcd-spi.h> // LCD_HEIGHT LCD_WIDTH
#include <libopencm3-plus/utils/misc.h>

// For touch screen (origin lower left)
#define MIN_X 350
#define MIN_Y 280
#define MAX_X 3650
#define MAX_Y 3800

// I2C Commands
#define CHIP_ID 0x00
#define ID_VER 0x02
#define SYS_CTRL1 0x03
#define SYS_CTRL2 0x04
#define TSC_CTRL 0x40
#define TSC_CFG 0x41
#define FIFO_TH 0x4A
#define FIFO_STA 0x4B
#define FIFO_SIZE 0x4C
#define TSC_DATA_X 0x4D
#define TSC_DATA_Y 0x4F
#define TSC_DATA_Z 0x51

// TSC_CTRL command
#define TSC_CTRL_INITIAL_VALUE 0xFF
uint8_t tsc_control_cfg = TSC_CTRL_INITIAL_VALUE;

void turn_clock(void);

void tft_setup(void) {
  const uint8_t MIN_FIFO_INTERRUPT_LEVEL_SIZE = 0x1;
  i2c_init();

  tft_enable_clocks(false);
  tft_touchscreen_enable_controller_control(false);

  tft_touchscreen_configure_controller_control(TSC_XYZ_AXIS, TRACK_8);
  tft_touchscreen_apply_controller_configuration(MICRO_S_500_SET, MICRO_S_500_D,
                                                 SAMPLE_2);
  tft_set_fifo_level_interrupt(MIN_FIFO_INTERRUPT_LEVEL_SIZE);

  tft_touchscreen_enable_controller_control(true);
  tft_enable_clocks(true);
}

// WIDTH -1 and HEIGHT - 1 => resolution
void tft_convert_touch_coord_to_lcd_coord(const int touch_x, int const touch_y,
                                          int *lcd_x, int *lcd_y) {
  *lcd_x =
      (touch_x - (int)MIN_X) * ((int)LCD_WIDTH - 1) / ((int)MAX_X - (int)MIN_X);
  if (*lcd_x > (int)LCD_WIDTH - 1) {
    *lcd_x = (int)LCD_WIDTH - 1;
  }
  if (*lcd_x < 0) {
    *lcd_x = 0;
  }
  *lcd_y = ((int)LCD_HEIGHT - 1) - (touch_y - (int)MIN_Y) *
                                       ((int)LCD_HEIGHT - 1) /
                                       ((int)MAX_Y - (int)MIN_Y);
  if (*lcd_y > (int)LCD_HEIGHT - 1) {
    *lcd_y = (int)LCD_HEIGHT - 1;
  }
  if (*lcd_y < 0) {
    *lcd_y = 0;
  }
}

uint16_t tft_identify_check_device(void) {
  uint8_t i2c_command = CHIP_ID;
  uint8_t dualVectorData[2];
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, &i2c_command, 1, dualVectorData,
                2);
  return (uint16_t)(dualVectorData[1] | (dualVectorData[0] << 8));
}

uint8_t tft_get_id_verification(void) {
  uint8_t i2c_command = ID_VER;
  uint8_t data = 0;
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, &i2c_command, 1, &data, 1);
  return data;
}

uint8_t tft_get_clock_status(void) {
  uint8_t i2c_command = SYS_CTRL2;
  uint8_t data = 0;
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, &i2c_command, 1, &data, 1);
  return data;
}

void turn_clock(void) {
  uint8_t data = 0;
  uint8_t i2c_dual_command[2];
  i2c_dual_command[0] = SYS_CTRL2;
  i2c_dual_command[1] = 0x00;
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, i2c_dual_command, 2, &data, 0);
}

void tft_enable_clocks(const bool askForEnable) {
  // NOTE: [brjmm] I don't know why this behaivor to enable/disable the clocks
  // The documentation, for clock system, is not working
  // Programming based on the experience
  static bool hasAskedFirstTime = false;
  static bool isCurrentStateEnable = false;
  const int waitTimeToResetClockSystem = 100;
  if (hasAskedFirstTime && isCurrentStateEnable) {
    if (!askForEnable) {
      turn_clock(); //disable
      isCurrentStateEnable = false;
    }
  } else if (hasAskedFirstTime && !isCurrentStateEnable) {
    if (askForEnable) {
      turn_clock(); //enable
      isCurrentStateEnable = true;
    }
  }
  if (!hasAskedFirstTime && !askForEnable) {
    turn_clock(); //disable
    hasAskedFirstTime = true;
    isCurrentStateEnable = false;
  } else if (!hasAskedFirstTime && askForEnable) {
    turn_clock(); //disable
    wait(waitTimeToResetClockSystem);
    turn_clock(); // enable
    hasAskedFirstTime = true;
    isCurrentStateEnable = true;
  }
}

uint8_t tft_get_controller_control_status(void) {
  uint8_t i2c_command = TSC_CTRL;
  uint8_t data = 0;
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, &i2c_command, 1, &data, 1);
  return data;
}

void tft_touchscreen_configure_controller_control(
    const tsc_control_opmode_t opmode, const tsc_control_track_t track) {
  uint8_t opmode_value = 0;
  switch (opmode) {
  case TSC_XYZ_AXIS:
    opmode_value = 0x00;
    break;
  case TSC_XY_AXIS:
    opmode_value = 0x01;
    break;
  case TSC_X_AXIS:
    opmode_value = 0x02;
    break;
  case TSC_Y_AXIS:
    opmode_value = 0x03;
    break;
  case TSC_Z_AXIS:
    opmode_value = 0x04;
    break;
  }
  uint8_t track_value = 0;
  switch (track) {
  case TRACK_NONE:
    track_value = 0x00;
    break;
  case TRACK_4:
    track_value = 0x01;
    break;
  case TRACK_8:
    track_value = 0x02;
    break;
  case TRACK_16:
    track_value = 0x03;
    break;
  case TRACK_32:
    track_value = 0x04;
    break;
  case TRACK_64:
    track_value = 0x05;
    break;
  case TRACK_92:
    track_value = 0x06;
    break;
  case TRACK_127:
    track_value = 0x07;
    break;
  }
  tsc_control_cfg = (track_value << 4) | (opmode_value << 1);
}

void tft_touchscreen_enable_controller_control(const bool askForEnable) {
  uint8_t i2c_dual_command[2];
  i2c_dual_command[0] = TSC_CTRL;
  const uint8_t enable_controller_action = askForEnable ? 0x01 : 0x00;
  // this condition is achieved when configuration has not been setting
  if ((uint8_t)TSC_CTRL_INITIAL_VALUE == tsc_control_cfg) {
    i2c_dual_command[1] = enable_controller_action;
  } else {
    i2c_dual_command[1] = tsc_control_cfg | enable_controller_action;
  }
  uint8_t data = 0;
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, i2c_dual_command, 2, &data, 0);
}

uint8_t tft_get_touchscreen_controller_configuration(void) {
  uint8_t i2c_command = TSC_CFG;
  uint8_t data = 0;
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, &i2c_command, 1, &data, 1);
  return data;
}

void tft_touchscreen_apply_controller_configuration(
    const tsc_configuration_settling_t settling,
    const tsc_configuration_delay_t delay,
    const tsc_configuration_ave_ctrl_t ave_ctrl) {
  const uint8_t initial_value = 0xFF;
  uint8_t settling_value = initial_value;
  switch (settling) {
  case MICRO_S_10_SET:
    settling_value = 0x00;
    break;
  case MICRO_S_100_SET:
    settling_value = 0x01;
    break;
  case MICRO_S_500_SET:
    settling_value = 0x02;
    break;
  case MILLI_S_1_SET:
    settling_value = 0x03;
    break;
  case MILLI_S_5_SET:
    settling_value = 0x04;
    break;
  case MILLI_S_10_SET:
    settling_value = 0x05;
    break;
  case MILLI_S_50_SET:
    settling_value = 0x06;
    break;
  case MILLI_S_100_SET:
    settling_value = 0x07;
    break;
  }
  uint8_t delay_value = initial_value;
  switch (delay) {
  case MICRO_S_10_D:
    delay_value = 0x00;
    break;
  case MICRO_S_50_D:
    delay_value = 0x01;
    break;
  case MICRO_S_100_D:
    delay_value = 0x02;
    break;
  case MICRO_S_500_D:
    delay_value = 0x03;
    break;
  case MILLI_S_1_D:
    delay_value = 0x04;
    break;
  case MILLI_S_5_D:
    delay_value = 0x05;
    break;
  case MILLI_S_10_D:
    delay_value = 0x06;
    break;
  case MILLI_S_50_D:
    delay_value = 0x07;
    break;
  }
  uint8_t ave_ctrl_value = initial_value;
  switch (ave_ctrl) {
  case SAMPLE_1:
    ave_ctrl_value = 0x00;
    break;
  case SAMPLE_2:
    ave_ctrl_value = 0x01;
    break;
  case SAMPLE_4:
    ave_ctrl_value = 0x02;
    break;
  case SAMPLE_8:
    ave_ctrl_value = 0x03;
    break;
  }
  uint8_t i2c_dual_command[2];
  i2c_dual_command[0] = TSC_CFG;
  if ((initial_value != settling_value) && (initial_value != delay_value) &&
      (initial_value != ave_ctrl_value)) {
    i2c_dual_command[1] =
        (ave_ctrl_value << 6) | (delay_value << 3) | settling_value;
  } else {
    i2c_dual_command[1] = 0x9A; // default config
  }
  uint8_t data = 0;
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, i2c_dual_command, 2, &data, 0);
}

uint8_t tft_get_fifo_interrupt_level(void) {
  uint8_t i2c_command = FIFO_TH;
  uint8_t data = 0;
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, &i2c_command, 1, &data, 1);
  return data;
}

void tft_set_fifo_level_interrupt(const uint8_t fifo_level_interrupt) {
  uint8_t i2c_dual_command[2];
  i2c_dual_command[0] = FIFO_TH;
  if (0 != fifo_level_interrupt) {
    i2c_dual_command[1] = fifo_level_interrupt;
  } else {
    i2c_dual_command[1] = 0x1; // default value
  }
  uint8_t data = 0;
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, i2c_dual_command, 2, &data, 0);
}

uint8_t tft_get_fifo_size(void) {
  uint8_t i2c_command = FIFO_SIZE;
  uint8_t data = 0;
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, &i2c_command, 1, &data, 1);
  return data;
}

void tft_get_coord_data_access(const tft_coord_t coord, uint16_t *coord_data) {
  uint8_t i2c_command = 0;
  switch (coord) {
  case X_COORD:
    i2c_command = TSC_DATA_X;
    break;
  case Y_COORD:
    i2c_command = TSC_DATA_Y;
    break;
  case Z_COORD:
    i2c_command = TSC_DATA_Z;
    break;
  }
  uint8_t dualVectorData[2];
  i2c_transfer7(STMPE811_I2C, STMPE811_ADDR, &i2c_command, 1, dualVectorData,
                2);
  switch (coord) {
  case X_COORD:
  case Y_COORD:
    *coord_data = dualVectorData[1] | (dualVectorData[0] << 8);
    break;
  case Z_COORD:
    *coord_data = dualVectorData[1];
    break;
  }
}

bool tft_is_touch_detected(void) {
  const uint8_t controller_status = tft_get_controller_control_status();
  const bool bit_detected = (controller_status >> 7) == 1 ? true : false;
  return bit_detected;
}
