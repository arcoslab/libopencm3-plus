/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2010 Thomas Otto <tommi@viadmin.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/i2c.h>
#include <libopencm3/stm32/rcc.h>

#include <libopencm3-plus/hw-accesories/tft_lcd/i2c-lcd-touch.h>

#define STMPE811_RCC RCC_I2C3
#define STM_SLAVE_ADDR 0x32

void i2c_init(void) {
  rcc_periph_clock_enable(STMPE811_RCC);
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOC);

  // i2c => GPIOA8: SCL, GPIOC9: SDA
  gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO8);
  gpio_mode_setup(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO9);
  gpio_set_af(GPIOA, GPIO_AF4, GPIO8);
  gpio_set_af(GPIOC, GPIO_AF4, GPIO9);

  i2c_reset(STMPE811_I2C);

  i2c_peripheral_disable(STMPE811_I2C);
  i2c_set_speed(STMPE811_I2C, i2c_speed_sm_100k, 36); //I2C_CR2_FREQ_36MHZ
  i2c_set_own_7bit_slave_address(STMPE811_I2C, STM_SLAVE_ADDR);
  i2c_peripheral_enable(STMPE811_I2C);
}
