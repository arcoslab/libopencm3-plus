/*
 * Copyright (C) 2013 ARCOS-Lab Universidad de Costa Rica
 * Author: Federico Ruiz Ugalde <memeruiz@gmail.com>
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <assert.h>
#include <libopencm3-plus/utils/misc.h>
#include <libopencm3-plus/hw-accesories/gde021a1.h>
#include <libopencm3-plus/hw-accesories/hw-misc.h>
#include <libopencm3-plus/utils/common.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>

/* void _update_bitfield_gde021a1(Gde021a1SPI dev, uint8_t reg, uint8_t bitfield, */
/*                       uint8_t value) { */
/*   uint8_t data; */
/*   uint8_t i; */
/*   sp1_read(dev, reg, &data, 1, true); */
/*   //  printf("%x\n", data); */
/*   // move new value to bitfield position */
/*   for (i = bitfield; (i & 0x01) == 0; i >>= 1) { */
/*     value <<= 1; */
/*   } */
/*   //  printf("%x\n", value); */
/*   // clean bitfield old data */
/*   data &= ~bitfield; */
/*   //  printf("%x\n", data); */
/*   // set new value on bitfield */
/*   data |= value; */
/*   //  printf("%x\n", data); */
/*   sp1_write(dev, reg, &data, 1); */
/* } */

/* uint8_t _get_bitfield_gde021a1(Gde021a1SPI dev, uint8_t reg, uint8_t bitfield) { */
/*   uint8_t data; */
/*   uint8_t i; */
/*   sp1_read(dev, reg, &data, 1, true); */
/*   data &= bitfield; */
/*   for (i = bitfield; (i & 0x01) == 0; i >>= 1) { */
/*     data >>= 1; */
/*   } */
/*   return (data); */
/* } */


uint8_t gde021a1_cmd_data(Gde021a1SPI spi_conf, uint8_t cmd,
                   uint8_t *wr_data, uint8_t count) {
  uint8_t status = 0x00;
  gpio_clear(spi_conf.spicsport, spi_conf.spi_cs);
  gpio_clear(spi_conf.dc_port, spi_conf.dc_pin);
  status = my_spi_xfer(spi_conf.spi, cmd);
  gpio_set(spi_conf.dc_port, spi_conf.dc_pin);
  for (int i = 0; i < count; i++) {
    my_spi_xfer(spi_conf.spi, *(wr_data + i));
  }
  gpio_set(spi_conf.spicsport, spi_conf.spi_cs);
  gpio_set(spi_conf.dc_port, spi_conf.dc_pin);
  return (status);
}

uint8_t gde021a1_data(Gde021a1SPI spi_conf,
                   uint8_t *wr_data, uint8_t count) {
  uint8_t status = 0x00;
  gpio_clear(spi_conf.spicsport, spi_conf.spi_cs);
  gpio_set(spi_conf.dc_port, spi_conf.dc_pin);
  for (int i = 0; i < count; i++) {
    status=my_spi_xfer(spi_conf.spi, *(wr_data + i));
  }
  gpio_set(spi_conf.spicsport, spi_conf.spi_cs);
  gpio_set(spi_conf.dc_port, spi_conf.dc_pin);
  return (status);
}

uint16_t gde021a1_busy(Gde021a1SPI spi_conf) {
  return(gpio_get(spi_conf.busy_port, spi_conf.busy_pin));
}


void init_gde021a1_spi(NOT_USED Gde021a1SPI dev) {
  printf("\n------------------\n");
  printf("Setting general GDE021A1 configuratio\n");
  printf("------------------\n");
}

void init_gde021a1(NOT_USED Gde021a1SPI dev) {
  printf("\n------------------\n");
  printf("Setting all general Gde021a1 configuration!\n");
  printf("------------------\n");
  printf(">>>\n");

}
