#include <libopencm3-plus/hw-accesories/lcd/gfx.h>
#include <libopencm3-plus/hw-accesories/lcd/lcd-spi.h>
#include <libopencm3-plus/utils/graph.h>
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void graph_create(uint16_t pixel_width,
                  uint16_t pixel_height,
                  graph_t *graph, uint16_t color,
                  uint16_t max_points) {
  graph->pixel_width=pixel_width;
  graph->pixel_height=pixel_height;
  graph->n_points=0;
  graph->x_max=0;
  graph->y_max=0;
  graph->x_min=0;
  graph->y_min=0;
  graph->color=color;
  graph->max_points=max_points;
  graph->points=malloc(sizeof(point_t)*max_points);
}

void graph_destroy(graph_t *graph) {
  graph->n_points=0;
  graph->color=0;
  graph->max_points=0;
  free(graph->points);
}

void graph_prune_half(graph_t *graph) {
  for (int i=0; i<(graph->n_points/2); i++) {
    graph->points[i]=graph->points[i*2];
  }
  graph->n_points/=2;
}

void graph_prune_half2(graph_t *graph) {
  float x_per_pixel;
  x_per_pixel=(graph->x_max-graph->x_min)/graph->pixel_width;
  int j=0;
  for (int i=0; i< graph->n_points; i++) {
    // for all existing points
    // check if current point x bigger than current expected x pixel point
    if ((*(graph->points+i)).x >= x_per_pixel*j*2) {
      // copy over whole point
      (*(graph->points+j))=(*(graph->points+i));
      j++;
    }
  }
  graph->n_points=j;
}

bool graph_add_point(float x, float y, graph_t *graph) {
  if (graph->n_points >= graph->max_points) {
    // full. rejecting point
    return(false);
  } else {
    (*(graph->points+graph->n_points)).x=x;
    (*(graph->points+graph->n_points)).y=y;
    graph->n_points+=1;
    if (x > graph->x_max) {
      graph->x_max=x;
    }
    if (x < graph->x_min) {
      graph->x_min=x;
    }
    if (y > graph->y_max) {
      graph->y_max=y;
    }
    if (y < graph->y_min) {
      graph->y_min=y;
    }
    return(true);
  }
}

// Converts data from real points to pixels
uint16_t p2pix(float data, float data_per_pixel) {
  return(rintf(data/data_per_pixel));
}

void graph_draw_borders(graph_t *graph, uint16_t x_pixel_pos,
                        uint16_t y_pixel_pos, uint16_t
                        x_pixel_div, uint16_t y_pixel_div,
                        float x_per_pixel, float y_per_pixel) {
  uint16_t x_div=x_pixel_div;
  uint16_t y_div=y_pixel_div;
  char scale_text[10];

  // borders
  gfx_drawRect(x_pixel_pos, y_pixel_pos, graph->pixel_width, graph->pixel_height, graph->color);
  // x axis divisions
  while (x_div < graph->pixel_width) {
    gfx_drawLine(x_pixel_pos+x_div, y_pixel_pos+graph->pixel_height-5, x_pixel_pos+x_div, y_pixel_pos+graph->pixel_height, graph->color);
    //gfx_setTextSize(LCD_TEXT_MIN_SIZE); // ?
    gfx_setTextSize(0); // ?
    gfx_setCursor(x_pixel_pos+x_div, y_pixel_pos+graph->pixel_height-13);
    sprintf(scale_text, "%d", (uint16_t) rintf(((float) x_div)*x_per_pixel));
    gfx_puts(scale_text);
    x_div+=x_pixel_div;
  }
  // y axis divisions
  while (y_div < graph->pixel_height) {
    gfx_drawLine(x_pixel_pos, y_pixel_pos+graph->pixel_height-y_div, x_pixel_pos+5, y_pixel_pos+graph->pixel_height-y_div, graph->color);
    gfx_setTextSize(0); // ?
    gfx_setCursor(x_pixel_pos+8, y_pixel_pos+graph->pixel_height-3-y_div);
    sprintf(scale_text, "%d", (uint16_t) rintf(((float) y_div)*y_per_pixel));
    gfx_puts(scale_text);
    y_div+=y_pixel_div;
  }
}

void graph_plot(graph_t *graph, uint16_t x_pixel_pos,
                uint16_t y_pixel_pos, float x_div,
                float y_div) {
  //printf("Plotting\n");
  float x_per_pixel, y_per_pixel;
  uint16_t x_pix_last, y_pix_last, x_pix, y_pix;
  x_per_pixel=(graph->x_max-graph->x_min)/graph->pixel_width;
  y_per_pixel=(graph->y_max-graph->y_min)/graph->pixel_height;

  graph_draw_borders(graph, x_pixel_pos, y_pixel_pos, p2pix(x_div, x_per_pixel), p2pix(y_div, y_per_pixel), x_per_pixel, y_per_pixel);

  //printf("N points %d\n", graph->n_points);
  // Initializing first x0, y0 for each line
  x_pix_last=p2pix(graph->points[0].x, x_per_pixel);
  y_pix_last=graph->pixel_height-1-p2pix(graph->points[0].y, y_per_pixel);
  for (int i=1; i<graph->n_points; i++) {
    //printf("%d\n", i);
    x_pix=p2pix(graph->points[i].x, x_per_pixel);
    y_pix=graph->pixel_height-1-p2pix(graph->points[i].y, y_per_pixel);
    //printf("Point %6d, Last: %6d %6d, New: %6d, %6d\n",i, x_pix_last, y_pix_last, x_pix, y_pix);
    gfx_drawLine(x_pixel_pos+x_pix_last, y_pixel_pos+y_pix_last, x_pixel_pos+x_pix, y_pixel_pos+y_pix, graph->color);
    gfx_drawLine(x_pixel_pos+x_pix_last, y_pixel_pos+y_pix_last+1, x_pixel_pos+x_pix, y_pixel_pos+y_pix+1, graph->color);
    //gfx_drawPixel(x_pixel_pos+x_pix, y_pixel_pos+y_pix, graph->color);
    x_pix_last=x_pix;
    y_pix_last=y_pix;
  }
}
