#include <stdlib.h>
#include <libopencm3-plus/utils/filters.h>

avg_filter_t  avg_filter_create_f(int size) {
  // Creates a filter data structured with size
  avg_filter_t avg_filter_data;
  avg_filter_data.data=(float*) malloc(sizeof(float)*size);
  avg_filter_data.size=size;
  return(avg_filter_data);
}

float avg_filter_f(avg_filter_t* avg_filter_data, float data) {
  static int ptr=0;
  static int cur_size=0;
  float filtered_output=0;
  avg_filter_data->data[ptr]=data;
  ptr+=1;
  if (ptr>avg_filter_data->size){
    ptr=0;
  }
  cur_size+=1;
  if (cur_size>avg_filter_data->size) {
    // filter is full
    cur_size=avg_filter_data->size;
  }
  for (int i=0; i<cur_size; i++) {
    filtered_output+=avg_filter_data->data[i];
  }
  return(filtered_output/cur_size);
}
